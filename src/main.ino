#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <ArduinoHttpClient.h>
#include <ArduinoJson.h>
#include <DNSServer.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <ezTime.h>
#include <Task.h>
#include <WiFiClientSecureBearSSL.h>
#include "WiFiManager.h"
#include <Wire.h>

#include <Fonts/FreeSansBold18pt7b.h>
#include <Fonts/FreeSans9pt7b.h>

#include "icons.h"

extern "C" {
#include "user_interface.h"
}

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64


WiFiManager wifiManager;

IPAddress broadcastIP;

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

TaskManager taskManager;

#define BRIGHTNESS_WINDOW 15
int brightnessValues[BRIGHTNESS_WINDOW] = {0};
char brightnessIndex = 0;

struct Weather {
    int temp = 20;
    int windSpeed = 3;
    char weather[21];
} weather;

void clockTask(uint32_t deltaTime);
void weatherTask(uint32_t deltaTime);
void brightnessTask(uint32_t deltaTime);
void httpUpdateTask(uint32_t deltaTime);

FunctionTask taskClock(clockTask, MsToTaskTime(1000));
FunctionTask taskWeather(weatherTask, MsToTaskTime(60 * 60 * 1000));
FunctionTask taskBrightness(brightnessTask, MsToTaskTime(50));
FunctionTask taskHTTPUpdate(httpUpdateTask, MsToTaskTime(24 * 60 * 60 * 1000));


String getTime() {
    Timezone Greece;
    Greece.setLocation("Europe/Athens");
    return Greece.dateTime("H:i");
}



void logUDP(String message, IPAddress ip) {
    if (WiFi.status() != WL_CONNECTED) {
        return;
    }

    WiFiUDP Udp;

    // Listen with `nc -kul 37243`.
    Udp.beginPacket(ip, 37243);
    Udp.write(("(" PROJECT_NAME ": " + String(millis()) + " - " + WiFi.localIP().toString() + ") " + ": " + message + "\n").c_str());
    Udp.endPacket();
}


void doHTTPUpdate() {
    digitalWrite(LED_BUILTIN, LOW);
    if (WiFi.status() != WL_CONNECTED) {
        return;
    }
    debug("[update] Looking for an update from v" VERSION ".");

    std::unique_ptr<BearSSL::WiFiClientSecure> client(new BearSSL::WiFiClientSecure);
    client->setFingerprint(OTA_FINGERPRINT);
    t_httpUpdate_return ret = ESPhttpUpdate.update(*client, "https://" OTA_HOSTNAME "/" PROJECT_NAME "/", VERSION);
    switch (ret) {
    case HTTP_UPDATE_FAILED:
        debug("[update] Update failed.");
        break;
    case HTTP_UPDATE_NO_UPDATES:
        debug("[update] No update from v" VERSION ".");
        break;
    case HTTP_UPDATE_OK:
        debug("[update] Update ok.");
        break;
    }
    digitalWrite(LED_BUILTIN, HIGH);
    debug("Ready.");
}


void connectToWiFi() {
    WiFi.hostname("DoNotBeAlarmed");
    wifiManager.setConfigPortalTimeout(5 * 60);
    wifiManager.autoConnect("Do not be alarmed");
    broadcastIP = WiFi.localIP();
    broadcastIP[3] = 255;
}


void showText(int row, String text) {
    if (row == 0) {
        display.clearDisplay();
    }
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0, 13 * row);

    // Display static text
    display.println(text.c_str());
    display.display();
}


void debug(const char* text) {
    Serial.println(text);
    logUDP(text, broadcastIP);
}


void drawCentreString(const char *buf, int x, int y) {
    int16_t x1, y1;
    uint16_t w, h;
    display.getTextBounds(buf, 0, 0, &x1, &y1, &w, &h);
    display.setCursor(x - w / 2, y + h / 2);
    display.print(buf);
}


void setBrightness(int brightness) {
    // Set the brightness of the SSD1306 OLED screen with this hack.
    // From https://www.youtube.com/watch?v=hFpXfSnDNSY.
    display.ssd1306_command(SSD1306_SETCONTRAST); // 0x81

    // `brightness` is a number from 0 to 255.
    display.ssd1306_command(brightness);

    display.ssd1306_command(SSD1306_SETPRECHARGE); // 0xD9
    display.ssd1306_command(0);

    display.ssd1306_command(SSD1306_SETVCOMDETECT); // 0xDB
    display.ssd1306_command(0);
}

void brightnessTask(uint32_t deltaTime) {
    int b = analogRead(A0);
    int brightness = constrain((b - 30) * 2, 0, 255);

    brightnessValues[brightnessIndex] = brightness;
    brightnessIndex = (brightnessIndex + 1) % BRIGHTNESS_WINDOW;

    int sum = 0;
    for (int i = 0; i < BRIGHTNESS_WINDOW; i++) {
        sum += brightnessValues[i];
    }
    int averageBrightness = sum / BRIGHTNESS_WINDOW;

    setBrightness(averageBrightness);
}

void getWeather() {
    debug("Getting weather...");
    WiFiClient wifi;
    HttpClient client = HttpClient(wifi, "api.openweathermap.org");
    client.get("/data/2.5/forecast?lat=" LAT "&lon=" LNG "&appid=" OWA_KEY "&units=metric&cnt=1");

    Serial.println("Weather!");
    DynamicJsonDocument doc(1000);
    deserializeJson(doc, client.responseBody());

    weather.temp = doc["list"][0]["main"]["feels_like"];
    weather.windSpeed = doc["list"][0]["wind"]["speed"]; // m/s
    strncpy(weather.weather, doc["list"][0]["weather"][0]["description"], sizeof(weather.weather));
    debug((("Weather is ") + String(weather.weather) + String(".")).c_str());
}

void weatherTask(uint32_t deltaTime) {
    getWeather();
}

void setDisplay() {
    display.clearDisplay();

    // Print the temperature.
    display.setFont(&FreeSans9pt7b);
    display.setTextSize(1);
    display.setCursor(0, 60);
    // We need to keep the String, otherwise the c_str won't stick around
    // long enough.
    String temp_str = String(weather.temp);
    const char *temp = temp_str.c_str();
    display.print(temp);

    // Print the little "o" to the right of the temperature.
    int16_t x1, y1;
    uint16_t w, h;
    display.getTextBounds(temp, 0, 0, &x1, &y1, &w, &h);

    display.setFont();
    display.setTextSize(1);
    display.setCursor(w + 3, 59 - h);
    display.print(String("o").c_str());

    // Print the wind speed.
    display.setFont(&FreeSans9pt7b);
    String wind_str = String(weather.windSpeed);
    const char *wind = wind_str.c_str();
    drawCentreString(wind, 54, 54);

    // Print the m/s unit icon.
    display.getTextBounds(wind, 0, 0, &x1, &y1, &w, &h);
    display.drawBitmap(62 + w, 47, EMES, 16, 16, 1);

    // Print the time.
    display.setFont(&FreeSansBold18pt7b);
    display.setTextSize(1);
    drawCentreString(getTime().c_str(), 64, 12);

    const unsigned char *ICON;
    if (strcmp(weather.weather, "clear sky") == 0) {
        ICON = ICON_CLEAR;
    } else if (strcmp(weather.weather, "few clouds") == 0) {
        ICON = ICON_FEW_CLOUDS;
    } else if (strcmp(weather.weather, "scattered clouds") == 0) {
        ICON = ICON_SCATTERED_CLOUDS;
    } else if (strcmp(weather.weather, "broken clouds") == 0) {
        ICON = ICON_BROKEN_CLOUDS;
    } else if (strcmp(weather.weather, "shower rain") == 0) {
        ICON = ICON_SHOWER_RAIN;
    } else if (strcmp(weather.weather, "rain") == 0) {
        ICON = ICON_RAIN;
    } else if (strcmp(weather.weather, "thunderstorm") == 0) {
        ICON = ICON_THUNDERSTORM;
    } else if (strcmp(weather.weather, "snow") == 0) {
        ICON = ICON_SNOW;
    } else if (strcmp(weather.weather, "mist") == 0) {
        ICON = ICON_MIST;
    } else {
        ICON = ICON_WAT;
    }
    display.drawBitmap(104, 47, ICON, 16, 16, 1);

    display.display();
}

void clockTask(uint32_t deltaTime) {
    if (!minuteChanged()) {
        return;
    }
    debug("Updating display...");

    setDisplay();
}

void httpUpdateTask(uint32_t deltaTime) {
    doHTTPUpdate();
}

void setup() {
    Serial.begin(115200);

    pinMode(A0, INPUT);

    if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
        // Address 0x3D for 128x64
        Serial.println(F("SSD1306 allocation failed"));
        for (;;)
            ;
    }
    delay(200);

    // Reduce brightness to a minimum so it doesn't shine in the night while rebooting.
    setBrightness(0);

    showText(0, "Connecting to WiFi...");
    connectToWiFi();
    debug("Connected.");

    showText(1, "Updating firmware...");
    doHTTPUpdate();
    showText(2, "No update available.");

    taskManager.StartTask(&taskClock);
    taskManager.StartTask(&taskWeather);
    taskManager.StartTask(&taskBrightness);
    taskManager.StartTask(&taskHTTPUpdate);

    getWeather();
    waitForSync(30);
    setDisplay();
}

void loop() {
    taskManager.Loop();
    delay(20);
    events();
}
